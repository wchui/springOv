package com.olva.eser.service;

import java.math.BigDecimal;

import com.olva.eser.entity.DocumentoIdentidad;

public interface IDocumentoIdentidadService {
	
	public DocumentoIdentidad findById(Integer id);
	
	public DocumentoIdentidad findByNumeroAndType(String numeroDocumento, String idTypeDoc);
	
}
