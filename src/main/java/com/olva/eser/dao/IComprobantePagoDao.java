package com.olva.eser.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.olva.eser.entity.ComprobantePago;

@Repository
public interface IComprobantePagoDao  extends JpaRepository<ComprobantePago, Long>{

}
