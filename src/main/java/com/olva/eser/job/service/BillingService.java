package com.olva.eser.job.service;

import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;


@Service
public class BillingService {

    private Logger log = LoggerFactory.getLogger(getClass());

    public static final long EXECUTION_TIME = 5000L;

    private AtomicInteger count = new AtomicInteger();

    public void callBillingProcess() {
    	int salto= 0;
        log.info("Ingresa al metodo.");
        try {
            Thread.sleep(EXECUTION_TIME);            
            
        } catch (InterruptedException e) {
            log.error("Error en job", e);
        } finally {
        	salto = getNumberOfInvocations();
            count.incrementAndGet();
            log.info("Job finalizado .." + salto);
            
        }
    }

    public int getNumberOfInvocations() {
        return count.get();  
    }
    

}
