package com.olva.eser;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import com.olva.eser.dto.PersonaJuridicaAreaDto;
import com.olva.eser.entity.DocumentoIdentidad;
import com.olva.eser.service.IComprobantePagoService;
import com.olva.eser.service.IDocumentoIdentidadService;
import com.olva.eser.service.IWsPagoEserService;
import com.olva.eser.util.Constante;

@SpringBootApplication
public class OlvaEserApplication {

    @Autowired
    private IComprobantePagoService comprobantePagoService;

    @Autowired
    private IDocumentoIdentidadService documentoIdentidadService;

    @Autowired
    private IWsPagoEserService eserService;
    
	static {	
	    javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
	    new javax.net.ssl.HostnameVerifier(){

	        public boolean verify(String hostname,
	                javax.net.ssl.SSLSession sslSession) {
	            if (hostname.equals("localhost")) {
	            	
	                return true;
	            }
	            return false;
	        }
	    });
	}
	
	public static void main(String[] args) {
		SpringApplication.run(OlvaEserApplication.class, args);
	}

	@EventListener(ApplicationReadyEvent.class)
	public void doSomethingAfterStartup() {
		//comprobantePagoService.updateCPOlvaCompras(new BigDecimal("3793560"), new BigDecimal("10939"));
		/*DocumentoIdentidad documentoIdentidad = documentoIdentidadService
		.findByNumeroAndType("12345", "A");
		
		System.out.println(documentoIdentidad .getNumeroDocumento());
		System.out.println(documentoIdentidad.getIdPersona().getIdPersona());
		
		PersonaJuridicaAreaDto pja = eserService.findByCodigoUno(
				documentoIdentidad.getIdPersona().getIdPersona(), Constante.ID_SEDE_LIMA);
		System.out.println(pja.getCodigo());*/
		
		
	}
	
}
